import { Component, OnInit } from '@angular/core';

import { ApiService } from '../../services/api.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  authResponse;
  feedResponse;

  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
  }

  onClickAuthButton() { // envoi requete pour authentification + store infos utilisateurs
    this.apiService.sendAuthenticationRequest().subscribe(rep => {
      this.apiService.setToken(rep[0]['access-token']);
      this.apiService.setUserInfo(rep[0]['profile']);
    }, error => {
      this.authResponse = error.message;
      console.log(error);
    });
  }

  onClickFeedButton() { // envoi requete pour dashboard
    this.apiService.sendDashboardRequest()
      .subscribe(rep => {
        // this.feedItems = rep;
      }, error => {
        this.feedResponse = error.message;
        console.log(error);
      });
  }

}
