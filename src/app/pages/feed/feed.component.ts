import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

import { ApiService } from '../../services/api.service';

import { items } from '../../mocks/items'



@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.css']
})
export class FeedComponent implements OnInit {

  constructor(private apiService: ApiService) { }

  feedItems: any = items; // temporaire (requete bloqué)

  ngOnInit(): void {
  }

  onClickLikeButtonItem(id) { // permet de thanks un item
    let indexOfItem;
    indexOfItem = this.findIndexOfObjectInArray(id, this.feedItems);

    this.feedItems[indexOfItem].thanx += 1;
  }

  onClickLikeButtonComment(idItem, idComment) { // permet de thanks un commentaire
    let indexOfItem;
    indexOfItem = this.findIndexOfObjectInArray(idItem, this.feedItems);

    let indexOfComment;
    indexOfComment = this.findIndexOfObjectInArray(idComment, this.feedItems[indexOfItem].commentaires);

    this.feedItems[indexOfItem].commentaires[indexOfComment].thanx += 1;
  }

  onEnter(text, id) { // permet de poster un commentaire sur un item
    let indexOfItem;
    indexOfItem = this.findIndexOfObjectInArray(id, this.feedItems);

    this.feedItems[indexOfItem].commentaires.push({
      id: this.feedItems[indexOfItem].commentaires.length + 1,
      avatar: "../../../assets/men2.png",
      nom: this.apiService.userProfile.name,
      text: text,
      time_posted: moment().format("DD/MM/YYYY HH:mm:ss"),
      thanx: 0
    });

    this.feedItems[indexOfItem].nb_commentaire += 1;
  }

  findIndexOfObjectInArray(objectId, array) { // recherche de l'index d'un objet dans un array à partir de son id
    return array.map(function (e) { return e.id; }).indexOf(objectId);
  }

  formatFromNowDate(strDate) { // calcul combien de temps s'est écoulé depuis date (format DD/MM/YYYY HH:mm:ss)
    if (typeof strDate == "undefined" || strDate == "") {
      return "";
    }
    else {
      return moment(strDate, 'DD/MM/YYYY HH:mm:ss').fromNow();
    }
  };


}
