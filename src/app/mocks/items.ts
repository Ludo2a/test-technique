export const items = [
    {
        "id": 1,
        "avatar": "../../../assets/men1.png",
        "nom": "Serge Koutouqn",
        "time_posted": "08/06/2020 18:00:00",
        "role": "Quditeurm Controleur de gestion junior",
        "hastag": [
            "finance",
            "controle de gestion",
            "audit"
        ],
        "text": "Le but de cet exercice est de refaire une petite partie du flux d'actualité de Wizbii en se basant sur l'API publique offerte par Wizbii.",
        "thanx": 0,
        "nb_commentaire": 1,
        "commentaires": [
            {
                "id": 1,
                "avatar": "../../../assets/woman.png",
                "nom": "Léa Beli",
                "text": "Je suis le commentaire",
                "time_posted": "08/06/2020 18:30:00",
                "thanx": 0
            }
        ]
    },
    {
        "id": 2,
        "avatar": "../../../assets/men1.png",
        "nom": "Serge Koutouqn",
        "time_posted": "08/06/2020 19:00:00",
        "role": "Quditeurm Controleur de gestion junior",
        "hastag": [
            "finance",
            "controle de gestion",
            "audit"
        ],
        "text": "Un exemple de ce qui est attendu en terme visuel est disponible à l'adresse https://remialvado.github.io/wizbii-technical-test/index.html",
        "thanx": 0,
        "nb_commentaire": 1,
        "commentaires": [
            {
                "id": 1,
                "avatar": "../../../assets/woman.png",
                "nom": "Léa Beli",
                "text": "Je suis le commentaire",
                "time_posted": "08/06/2020 19:30:00",
                "thanx": 0
            }
        ]
    },
    {
        "id": 3,
        "avatar": "../../../assets/men1.png",
        "nom": "Serge Koutouqn",
        "time_posted": "08/06/2020 20:00:00",
        "role": "Quditeurm Controleur de gestion junior",
        "hastag": [
            "finance",
            "controle de gestion",
            "audit"
        ],
        "text": "Cette API est sécurisée au moyen du protocole oAuth2.0 et est donc facilement compréhensible par tout Développeur Web.",
        "thanx": 0,
        "nb_commentaire": 1,
        "commentaires": [
            {
                "id": 1,
                "avatar": "../../../assets/woman.png",
                "nom": "Léa Beli",
                "text": "Je suis le commentaire",
                "time_posted": "08/06/2020 20:30:00",
                "thanx": 0
            }
        ]
    }
]