import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  url: string = 'https://api.wizbii.com/';
  token: string = '4fce1edf2c6e50605b307ebcad5f86f917d2ff2bceccdbbba28b4ecef64ba087'; // temporaire (requete bloqué)

  constructor(private http: HttpClient) { }

  sendAuthenticationRequest() { // requete permettant l'authentification
    let headers = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded');

    let body = {
      username: 'decouverte%2B2%40wizbii.com',
      password: 'decouverte',
      client_id: 'test',
      grant_type: 'password'
    }

    return this.http.post(this.url + 'v1/account/validate', body, { headers: headers });
  }

  sendDashboardRequest() { // requete pour recuperer les items pour le feed
    let headers = new HttpHeaders()
      .set('Authorization', 'Bearer ' + this.token);

    let body = {}

    return this.http.post(this.url + 'v2/dashboard/?direction=newest', body, { headers: headers });
  }

  setToken(token) {
    this.token = token;
  }

  setUserInfo(data) {
    this.userProfile = data;
  }

  userProfile = { // temporaire (requete bloqué)
    "_id": "serge-papagali",
    "name": "Serge Papagali",
    "first_connection_i_o_s_step": 0,
    "first_connection_i_o_s_step_name": "about_you",
    "first_connection_android_step_name": "about_you",
    "slug": "serge-papagali",
    "date_created": "2018-12-06T12:40:44+0000",
    "date_modified": "2019-09-19T12:02:33+0000",
    "is_no_index": false,
    "deleted": false,
    "language": "fr",
    "locale": "fr_FR",
    "original_locale": "fr_FR",
    "first_name": "Serge",
    "last_name": "Papagali",
    "sex": "HOMME",
    "mobile": "098765445678",
    "status": "ACTIF",
    "date_birthday": "1988-03-05T00:00:00+0000",
    "filling_ratio": 50,
    "filling_ratio_empty_info": {
      "enabled": 1,
      "avatar": 1,
      "resume": 1,
      "title": 1,
      "skills": 1,
      "skills_soft": 1,
      "languages": 1,
      "experiences_pro": 1
    }
  };

}
