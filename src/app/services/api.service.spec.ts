import { TestBed } from '@angular/core/testing';

import { HttpClientModule } from "@angular/common/http";

import { FeedComponent } from '../pages/feed/feed.component';
import { LoginComponent } from '../pages/login/login.component';
import { ApiService } from './api.service';

describe('ApiService', () => {
  let service: ApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FeedComponent, LoginComponent],
      imports: [HttpClientModule]
    });
    service = TestBed.inject(ApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
